# instascrap

Shows who not following you from your followers (not followback).

## How to run

Specify environment variables

- INSTASCRAP_USER - username for auth
- INSTASCRAP_PASSWORD - password for auth
- INSTASCRAP_TARGET - insta nickname of suspect :)

Then just run `main.go`.
