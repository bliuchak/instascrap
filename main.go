package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/bliuchak/goinsta"
)

func main()  {
	username, exist := os.LookupEnv("INSTASCRAP_USER")
	if !exist {
		log.Fatal("Env var INSTASCRAP_USER not set")
	}

	password, exist := os.LookupEnv("INSTASCRAP_PASSWORD")
	if !exist {
		log.Fatal("Env var INSTASCRAP_PASSWORD not set")
	}

	target, exist := os.LookupEnv("INSTASCRAP_TARGET")
	if !exist {
		log.Fatal("Env var INSTASCRAP_TARGET not set")
	}


	inst := goinsta.New(username, password)
	if err := inst.Login(); err != nil {
		log.Fatal("Unable to login goinsta.New()")
	}

	user, err := inst.Profiles.ByName(target)
	if err != nil {
		log.Fatal("Unable to get target data")
	}

	followersData := make(map[int64]string)
	followers := user.Followers()
	for followers.Next() {
		for _, user := range followers.Users {
			followersData[user.ID] = user.Username
		}
	}

	followingData := make(map[int64]string)
	following := user.Following()
	for following.Next() {
		for _, user := range following.Users {
			followingData[user.ID] = user.Username
		}
	}

	var result []string
	for id, username := range followingData {
		_, ok := followersData[id]
		if !ok {
			result = append(result, username)
		}
	}

	fmt.Println(strings.Join(result, ", "), len(result))
}
